<?php  
     
namespace App\Http\Controllers;  
    
use Illuminate\Http\Request;  
use App\Exports\UsersExport;  
use App\Imports\UsersImport;  
use Maatwebsite\Excel\Facades\Excel;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Models\Student;  
    
class MyController extends Controller  
{  
    /** 
    * It will return \Illuminate\Support\Collection 
    */  
    public function importExportView()  
    {  
       return view('import');  
    }  
     
    /** 
    * It will return \Illuminate\Support\Collection
    */  
    public function export()
    {  
        return Excel::download(new UsersExport, 'multisheet.xls');  
    }  
     
    /** 
    * It will return \Illuminate\Support\Collection 
    */  
    public function import()
    {  
        User::truncate();
        Student::truncate();
        Excel::import(new UsersImport,request()->file('file'));
        return back()->with('success','Data imported Successfully!');
    }  

    public function userList(Request $request)
    {
     
        $user= DB::table('users')->paginate(1);
        return view('userList',['users'=>$user]);

    }
}