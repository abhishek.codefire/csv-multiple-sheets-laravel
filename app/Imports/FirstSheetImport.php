<?php
namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsOnError;

class FirstSheetImport implements ToModel,WithHeadingRow ,WithValidation,SkipsOnError
{
    use Importable;
    public function model(array $row)
    {
        return new User([
            'first_name'     => $row['first_name'],
            'last_name'     => $row['last_name'],
            'email'    => $row['email'], 
            'phone'    => $row['phone'], 
          
        ]);
    }

    public function rules(): array
    {
        return [
        '*.email' => ['email','required','unique:users,email']
        ];
    }

    public function OnError(\Throwable $error)
    {
        
    }
}