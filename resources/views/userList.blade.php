<!DOCTYPE html>
<html>
<head>
  <title>View Data</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  <style>
    table,th,td{
        text-align:center;
    }
    .w-5{
      display:none;
    }
    </style>
</head>
<body>
<div class="container"><br>
    <h3>User List</h3>
  <div class="input-group mb-3">
    <table class="table table-bordered table table-hover  table-striped">
      <thead class="table table-dark">
        <tr>
          <th>id</th>
          <th>FirstName</th>
          <th>LastName</th>
          <th>Email</th>
          <th>Phone</th>
        </tr>
      </thead>
      <tbody>
      @foreach($users as $user)
        <tr>
          <td>{{$user->id}}</td>
          <td>{{$user->first_name}}</td>
          <td>{{$user->last_name}}</td>
          <td>{{$user->email}}</td>
          <td>{{$user->phone}}</td>
        </tr>
        @endforeach
        <a class="btn btn-info" style="margin:20px" href="{{ route('View') }}">Back</a>
      </tbody>
  
    </table>
    {{$users->links()}}  
  </div>
</body>
</html>